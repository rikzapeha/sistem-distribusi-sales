/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author rikza
 */
public class MasterKonversiMT {

    private int idmkonverter;
    private String distributor;

    private int depo;
    private int distributorIndex;
    private int tanggal;
    private int closing;
    private int nota;
    private int kodeCustomer;
    private int namaCustomer;
    private int frekuensi;
    private int alamat;
    private int pasar;
    private int kecamatan;
    private int kabupaten;
    private int kodeBarang;
    private int namaBarang;
    private int satuanProduk;
    private int penjualan;
    private int namaSalesman;
    private int rupiah;
    private int dos;
    private int pak;
    private int pcs;

    private int idMConverterForVersi;
    private String namaVersi;

    private static int idmkonverter_selected;
    private static String distributor_selected;
    private static int distributorIndex_selected;
    private static int idDistributor_selected;
    private static int depo_selected;
    private static int idDepo_selected;
    private static int closing_selected;
    private static int tanggal_selected;
    private static int penjualan_selected;

    private static int nota_selected;
    private static int kodeCustomer_selected;
    private static int namaCustomer_selected;
    private static int frekuensi_selected;
    private static int alamat_selected;
    private static int pasar_selected;
    private static int kecamatan_selected;
    private static int kabupaten_selected;
    private static int kodeBarang_selected;
    private static int namaBarang_selected;
    private static int satuanProduk_selected;
    private static int namaSalesman_selected;
    private static int rupiah_selected;
    private static int dos_selected;
    private static int pak_selected;
    private static int pcs_selected;

    public int getIdMKonverter() {
        return idmkonverter;
    }

    public String getIdMKonverterToString() {
        String idToString = Integer.toString(idmkonverter);
        return idToString;
    }

    public void setidMKonverter(int id) {
        this.idmkonverter = id;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public int getDistributorIndex() {
        return distributorIndex;
    }

    public void setDistributorIndex(int distributorIndex) {
        this.distributorIndex = distributorIndex;
    }

    public int getDepo() {
        return depo;
    }

    public void setDepo(int depo) {
        this.depo = depo;
    }

    public int getClosing() {
        return closing;
    }

    public void setClosing(int closing) {
        this.closing = closing;
    }

    public int getTanggal() {
        return tanggal;
    }

    public void setTanggal(int tanggal) {
        this.tanggal = tanggal;
    }

    public int getPenjualan() {
        return penjualan;
    }

    public void setPenjualan(int penjualan) {
        this.penjualan = penjualan;
    }

    public int getIdMConverterForVersi() {
        return idMConverterForVersi;
    }

    public void setConverterForVersi(int converterForVersi) {
        this.idMConverterForVersi = converterForVersi;
    }

    public String getNamaVersi() {
        return namaVersi;
    }

    public void setNamaVersi(String namaVersi) {
        this.namaVersi = namaVersi;
    }

    @Override
    public String toString() {
        String distributorDanVersi = "";
        try {
            distributorDanVersi = distributor + " VERSI " + namaVersi;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return distributorDanVersi;
    }

    public int toInt() {
        return idmkonverter;
    }

//    SELECTED ============================================================
    public int getIdMKonverterSelected() {
        return idmkonverter_selected;
    }

    public String getIdMKonverterToStringSelected() {
        String idToString = Integer.toString(idmkonverter);
        return idToString;
    }

    public void setidDistributorSelected(int id) {
        this.idDistributor_selected = id;
    }

    public int getIdDistributorSelected() {
        return idDistributor_selected;
    }

    public void setidMKonverterSelected(int id) {
        this.idmkonverter_selected = id;
    }

    public String getDistributorSelected() {
        return distributor_selected;
    }

    public void setDistributorSelected(String distributor) {
        this.distributor_selected = distributor;
    }

    public int getDistributorIndexSelected() {
        return distributorIndex_selected;
    }

    public void setDistributorIndexSelected(int distributorIndex) {
        this.distributorIndex_selected = distributorIndex;
    }

    public int getIdDepoSelected() {
        return idDepo_selected;
    }

    public void setIdDepoSelected(int idDepo) {
        this.idDepo_selected = idDepo;
    }

    public int getDepoSelected() {
        return depo_selected;
    }

    public void setDepoSelected(int depo) {
        this.depo_selected = depo;
    }

    public int getClosingSelected() {
        return closing_selected;
    }

    public void setClosingSelected(int closing) {
        this.closing_selected = closing;
    }

    public int getTanggalSelected() {
        return tanggal_selected;
    }

    public void setTanggalSelected(int tanggal) {
        this.tanggal_selected = tanggal;
    }

    public int getPenjualanSelected() {
        return penjualan_selected;
    }

    public void setPenjualanSelected(int penjualan) {
        this.penjualan_selected = penjualan;
    }

    public int getNotaSelected() {
        return nota_selected;
    }

    public void setNotaSelected(int nota) {
        this.nota_selected = nota;
    }

    public int getKodeCustomerSelected() {
        return kodeCustomer_selected;
    }

    public void setKodeCustomerSelected(int kodeCustomer) {
        this.kodeCustomer_selected = kodeCustomer;
    }

    public int getNamaCustomerSelected() {
        return namaCustomer_selected;
    }

    public void setNamaCustomerSelected(int namaCustomer) {
        this.namaCustomer_selected = namaCustomer;
    }

    public int getFrekuensiSelected() {
        return frekuensi_selected;
    }

    public void setFrekuensiSelected(int frekuensi) {
        this.frekuensi_selected = frekuensi;
    }

    public int getAlamatSelected() {
        return alamat_selected;
    }

    public void setAlamatSelected(int alamat) {
        this.alamat_selected = alamat;
    }

    public int getPasarSelected() {
        return pasar_selected;
    }

    public void setPasarSelected(int pasar) {
        this.pasar_selected = pasar;
    }

    public int getKecamatanSelected() {
        return kecamatan_selected;
    }

    public void setKecamatanSelected(int kecamatan) {
        this.kecamatan_selected = kecamatan;
    }

    public int getKabupatenSelected() {
        return kabupaten_selected;
    }

    public void setKabupatenSelected(int kabupaten) {
        this.kabupaten_selected = kabupaten;
    }

    public int getKodeBarangSelected() {
        return kodeBarang_selected;
    }

    public void setKodeBarangSelected(int kodeBarang) {
        this.kodeBarang_selected = kodeBarang;
    }

    public int getNamaBarangSelected() {
        return namaBarang_selected;
    }

    public void setNamaBarangSelected(int namaBarang) {
        this.namaBarang_selected = namaBarang;
    }

    public int getSatuanProdukSelected() {
        return satuanProduk_selected;
    }

    public void setSatuanProdukSelected(int satuanProduk) {
        this.satuanProduk_selected = satuanProduk;
    }

    public int getNamaSalesmanSelected() {
        return namaSalesman_selected;
    }

    public void setNamaSalesmanSelected(int namaSalesman) {
        this.namaSalesman_selected = namaSalesman;
    }

    public int getRupiahSelected() {
        return rupiah_selected;
    }

    public void setRupiahSelected(int rupiah) {
        this.rupiah_selected = rupiah;
    }

    public int getDosSelected() {
        return dos_selected;
    }

    public void setDosSelected(int dos) {
        this.dos_selected = dos;
    }

    public int getPakSelected() {
        return pak_selected;
    }

    public void setPakSelected(int pak) {
        this.pak_selected = pak;
    }

    public int getPcsSelected() {
        return pcs_selected;
    }

    public void setPcsSelected(int pcs) {
        this.pcs_selected = pcs;
    }

}
