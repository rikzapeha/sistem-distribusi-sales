/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rikza
 */
public class DistributorModelMT {

    Connection connection;
    PreparedStatement preparedStatement;

    private int id_distributor;
    private String nama_distributor;
    private String propinsi;
    private String kodeerp;
    private String satuanitem;
    private String alias;

    Map<String, String> listSatuan = new HashMap<>();
    static List<Map.Entry<String, String>> listSatuanProduk;

    Map<String, String> listVersi = new HashMap<>();
    static List<Map.Entry<String, String>> listVersiDist;

    public DistributorModelMT(Connection connection) {
        this.connection = connection;
    }

    public String selectTable(String namaDistributor) throws SQLException {
        preparedStatement = connection.prepareStatement("SELECT * FROM distributormt "
                + "WHERE nama_distributor like '%" + namaDistributor + "%' LIMIT 1");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            this.id_distributor = rs.getInt(1);
            System.out.println(id_distributor);
            this.nama_distributor = rs.getString(2);
            this.propinsi = rs.getString(3);
            this.kodeerp = rs.getString(4);
            this.satuanitem = rs.getString(5);
            this.alias = rs.getString(6);
        }
        return "";
    }

    public String getNamaDistribbutor() {
        return this.nama_distributor;
    }

    public String getSatuanItem() {
        return this.satuanitem;
    }

    public int getIdDistributor() {
        return this.id_distributor;
    }

    public void listAllSatuanOfDistributor() {
        String sqlNameAndSatuanDistributor = "";
        try {
            sqlNameAndSatuanDistributor = "SELECT nama_distributor, satuanitem "
                    + "FROM distributormt";
            preparedStatement = connection.prepareStatement(sqlNameAndSatuanDistributor);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                listSatuan.put(rs.getString("nama_distributor"), rs.getString("satuanitem"));
            }

            Set<Map.Entry<String, String>> set = listSatuan.entrySet();
            listSatuanProduk = new ArrayList<>(set);

            for (int i = 0; i < listSatuanProduk.size(); i++) {
                System.out.println(listSatuanProduk.get(i).getKey() + ": "
                        + listSatuanProduk.get(i).getValue());
            }
        } catch (Exception e) {
            System.out.println(e.getSuppressed());
        }
    }

    public String getSatuanProdukDariDistributor(String namaDistributor) {
        String satuanProduk = "";
        try {
            satuanProduk = listSatuan.get(namaDistributor);
        } catch (Exception e) {
            System.out.println(e.getSuppressed());
        }
        return satuanProduk;
    }

    public void listVersi(String namaDistributor) {
        String sqlListVersi = "";
        try {
            sqlListVersi = "SELECT id_m_converter, namaversi "
                    + "FROM m_converter WHERE distributormt LIKE '" + namaDistributor + "'";
            System.out.println(sqlListVersi);
            preparedStatement = connection.prepareStatement(sqlListVersi);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                listVersi.put(rs.getString("id_m_converter"), rs.getString("namaversi"));
            }

            Set<Map.Entry<String, String>> set = listVersi.entrySet();
            listVersiDist = new ArrayList<>(set);
//            System.out.println("TAMPIL VERSI : ");
            for (int i = 0; i < listVersiDist.size(); i++) {
                System.out.println(listVersiDist.get(i).getKey() + ": "
                        + listVersiDist.get(i).getValue());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
