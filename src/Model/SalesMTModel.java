/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author rikza
 */
public class SalesMTModel {

    private static String idSesiImport = "";

    Connection connection;
    PreparedStatement preparedStatement;

    public SalesMTModel(Connection connection) {
        this.connection = connection;
    }

    public void insertSales(String sql_qry) {
        try {

        } catch (Exception e) {
        }
    }

    public void setIdSesiImport(String action) {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String strDate = dateFormat.format(date);
        Random angkaAcak = new Random();
        try {
            if (action.equalsIgnoreCase("set")) {
                this.idSesiImport = strDate.toString() + "" + angkaAcak.nextInt(99);
            } else if (action.equalsIgnoreCase("reset")) {
                this.idSesiImport = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getIdSesiImport() {
        return idSesiImport;
    }

    public String deleteSalesFromIDSesiImport(String idSesi) {
        String sql_delete;
        String msg = "";
        try {
            if (!idSesi.equalsIgnoreCase("")) {
                sql_delete = "DELETE FROM salesmt WHERE id_sesi_import = '" + idSesi + "'";
                System.out.println(sql_delete);
                PreparedStatement preparedStmt = connection.prepareStatement(sql_delete);
                preparedStmt.execute(sql_delete);
                msg = "Success Delete";
            }
        } catch (Exception e) {
            msg = "Error Delete";
        }
        return msg;
    }
}
