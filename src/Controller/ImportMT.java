package Controller;

import Model.DistributorModel;
import Model.DistributorModelMT;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Iterator;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import View.FrameBaru;
import Model.MasterKonversi;
import Model.MasterKonversiMT;
import Model.SalesMTModel;
import Model.SalesModel;
import View.FrameMT;
import com.mysql.jdbc.StringUtils;
import example.RikzaTesting;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.DateUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import utility.KoneksiDB;

public class ImportMT extends SwingWorker<Void, Void> {

    private String lokasi, datanya, SQL_Insert;
    private String[] DataToInsert;
    private int i, numRow, input=0;
    public JOptionPane mohonTungguDialog = new JOptionPane();
    FrameMT frBaru = new FrameMT();
    MasterKonversiMT mk;
    MasterKonversiMTService mks;
    SalesMTModel sm;
    private String idDepoMasterKonverter;
    private Connection con;
    private Statement s;
    DistributorModelMT mDistributor;
    static Stack<String> stackAllDepo = new Stack<String>();
    static Stack<String> stackAllIDDepo = new Stack<String>();

    Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
//    private String catatDepoPerbaruan[];
    private Stack<String> stackDepoPerbaruan = new Stack<String>();

    public ImportMT(String fileExcel) throws SQLException {
        con = KoneksiDB.getKoneksi();
//        frBaru = new FrameMT();
        this.lokasi = fileExcel;
    }

    @Override
    protected Void doInBackground() throws Exception {
        mohonTunggu();
        frBaru.setbtnUploadText("SEDANG PROSES");
        String satuanProduk = "";
//        con.setAutoCommit(false);
        try {
//            con.setAutoCommit(false);
            s = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            datanya = "";
            /* proses membaca file excel */
            InputStream ExcelFileToRead = new FileInputStream(lokasi);
            XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
            XSSFSheet sheet = wb.getSheetAt(0);
            numRow = sheet.getPhysicalNumberOfRows();
            XSSFRow row;
            XSSFCell cell;
            Iterator rows = sheet.rowIterator();

            mk = new MasterKonversiMT();
            mks = new MasterKonversiMTService(con);
            sm = new SalesMTModel(con);

            mks.getKonverter(Integer.parseInt(frBaru.getIdMKonverter()));
            mDistributor = new DistributorModelMT(con);
            mDistributor.selectTable(mk.getDistributorSelected());
            getAllDepo(mDistributor.getIdDistributor());
            if (isClosing(frBaru.getBulanClosing(), frBaru.getTahunClosing()) == true) {
                JOptionPane.showMessageDialog(null, "DATA INI SUDAH CLOSING, JADI TIDAK DAPAT MEMPERBARUI DATA", "DANGER", JOptionPane.ERROR_MESSAGE);
                System.exit(100);
            } else {
//            System.out.println("DEPO SEMUA : " + stackAllDepo);
//            System.out.println("ID DEPO SEMUA : " + stackAllIDDepo);
                while (rows.hasNext()) {
                    input++;
                    row = (XSSFRow) rows.next();
                    for (int i = 0; i < row.getLastCellNum(); i++) {
                        cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
                        switch (cell.getCellType()) {
                            case Cell.CELL_TYPE_BLANK:
                                datanya += "NULL~";
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                    String hasil = df.format(cell.getDateCellValue());
                                    datanya += hasil + "~";
                                } else {
                                    DecimalFormat df = new DecimalFormat("###.###");
                                    String hasil = df.format(cell.getNumericCellValue());
                                    datanya += hasil + "~";
                                }
                                break;
                            case Cell.CELL_TYPE_STRING:
                                datanya += cell.getStringCellValue().toString() + "~";
                                break;
                        }

                    }
                    /* Proses Insert ke database */
                    DataToInsert = datanya.split("~");
                    new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                    System.out.println("");
                    System.out.println("Baris-" + input + " datanya : " + datanya);
                    System.out.println("");
                    if (mk.getSatuanProdukSelected() != 0) {
                        satuanProduk = DataToInsert[mk.getSatuanProdukSelected()].trim();
                    } else {
                        satuanProduk = mDistributor.getSatuanItem();
                    }
                    System.out.println("Satuan Produknya : " + satuanProduk);
                    System.out.println("DEPO : " + hapusCharacter(DataToInsert[mk.getDepoSelected()].trim()));
                    if (!hapusCharacter(DataToInsert[mk.getDepoSelected()].trim()).equalsIgnoreCase("NULL")) {
                        Map DataDepo = cekAvailableDepo(DataToInsert[mk.getDepoSelected()].trim());
                        System.out.println("DATADEPO: " + DataDepo);
                        if (DataDepo.get("available").equals("false")) {
                            JOptionPane.showMessageDialog(null, "[BARIS KE - "+ input +"] DEPO " + DataDepo.get("nama_wilayah") + " TIDAK TERDAFTAR DI MASTER, MOHON CEK DEPO DI MASTER WILAYAH", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
                            break;
                        } else {

                            System.out.println("DEPO TIDAK NULL: " + hapusCharacter(DataToInsert[mk.getDepoSelected()].trim()));

                            SQL_Insert = "INSERT INTO salesmt (id_sesi_import, distributor, id_distributor, depo, id_depo, id_barang, tanggal, closing,"
                                    + "nota, kode_customer, nama_customer, frekuensi, alamat, pasar, kecamatan,"
                                    + "kabupaten, kode_barang, nama_barang, satuan_produk, penjualan, nama_salesman, rupiah, tahun_closing,penjualand,kode_barangd,nama_barangd,satuan_produkd,dos,pak,pcs)"
                                    + " VALUES(";
                            SQL_Insert += "'" + sm.getIdSesiImport() + "',";
                            SQL_Insert += "'" + mDistributor.getNamaDistribbutor() + "',";
                            SQL_Insert += "'" + mDistributor.getIdDistributor() + "',";
//                        SQL_Insert += "'" + hapusCharacter(DataToInsert[mk.getDepoSelected()].trim()) + "',";
                            SQL_Insert += "'" + DataDepo.get("nama_wilayah") + "',";
                            SQL_Insert += "'" + DataDepo.get("id_wilayah") + "',";
                            Map APIValidasiKodeDanSatuan = kodeBarangTersediaDiKonversiProduk(DataToInsert[mk.getKodeBarangSelected()].trim(), satuanProduk, DataToInsert[mk.getPenjualanSelected()].trim());
                            SQL_Insert += "'" + APIValidasiKodeDanSatuan.get("idmi") + "',";
                            tambahDepoPerbaruan(DataToInsert[mk.getDepoSelected()].trim());

                            SQL_Insert += "'" + DataToInsert[mk.getTanggalSelected()] + "',";
                            SQL_Insert += "'" + (frBaru.getBulanClosing()) + "',";
                            SQL_Insert += "'" + hapusCharacter(DataToInsert[mk.getNotaSelected()].trim()) + "',";
                            SQL_Insert += "'" + hapusCharacter(DataToInsert[mk.getKodeCustomerSelected()].trim()) + "',";
                            SQL_Insert += "'" + hapusCharacter(DataToInsert[mk.getNamaCustomerSelected()].trim()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getFrekuensiSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getAlamatSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getPasarSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getKecamatanSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getKabupatenSelected()) + "',";

                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getKodeBarangSelected()) + "',";
//                    SQL_Insert += "'" + APIValidasiKodeDanSatuan.get("kodebarangmi").toString() + "',";
//                    System.out.println("KODE BARANG: " + cekJanganMiripIndexDepo(mk.getKodeBarangSelected()));

                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getNamaBarangSelected()) + "',";
//                    SQL_Insert += "'" + APIValidasiKodeDanSatuan.get("namabarangmi").toString() + "',";
                            SQL_Insert += "'" + APIValidasiKodeDanSatuan.get("satuan").toString() + "',";
//                    SQL_Insert += "'" + cekJanganMiripIndexDepoNol(mk.getPenjualanSelected()).replace(',', '.') + "',";
                            SQL_Insert += "'" + APIValidasiKodeDanSatuan.get("qtykarton").toString().replace(',', '.') + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getNamaSalesmanSelected()) + "',";
                            if (mk.getRupiahSelected() == 0) {
                                SQL_Insert += "0,";
                            } else {
                                SQL_Insert += "" + DataToInsert[mk.getRupiahSelected()].trim().replace(',', '.') + ",";
                            }
                            SQL_Insert += "'" + frBaru.getTahunClosing() + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepoNol(mk.getPenjualanSelected()).replace(',', '.') + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getKodeBarangSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepo(mk.getNamaBarangSelected()) + "',";
                            SQL_Insert += "'" + satuanProduk + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepoNol(mk.getDosSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepoNol(mk.getPakSelected()) + "',";
                            SQL_Insert += "'" + cekJanganMiripIndexDepoNol(mk.getPcsSelected()) + "'";
                            SQL_Insert += ")";
                            System.out.println(SQL_Insert);
                            s.executeUpdate(SQL_Insert);
//                        s.close();
                            datanya = "";
                            SQL_Insert = "";
                        }
                    }
                }
                if (frBaru.getSelectedJenisData().equalsIgnoreCase("AKUMULATIF")) {
                    System.out.println(frBaru.getSelectedJenisData());
                    System.out.println("AKAN MENGHAPUS DATA DEPO YANG LAMA");
                    this.hapusDataLamaBerdepo();
                } else if (frBaru.getSelectedJenisData().equalsIgnoreCase("DAILY")) {
                    System.out.println(frBaru.getSelectedJenisData());
                    System.out.println("TIDAK MENGHAPUS DATA DEPO YANG LAMA");
                } else {
                    System.out.println("GOLPUT");
                }
                dosPakPcsToKarton(frBaru.getBulanClosing(), frBaru.getTahunClosing());
            }

        } catch (Exception err) {
            sm.deleteSalesFromIDSesiImport(sm.getIdSesiImport());
            err.printStackTrace();
            if (err.getMessage().indexOf("IndexOutOfBoundsException") != -1) {
                JOptionPane.showMessageDialog(null,"[BARIS KE - "+ input +"] Error Pada Converter Distributor, mohon untuk dicek lagi peletakan kolomnya", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Kolom " + err.getMessage() + " Pada Data Excel Baris ke-" + input + " Tidak Dapat Ditemukan", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
            }
            JOptionPane.showMessageDialog(null, "Data yg sudah terekam di database telah dihapus karena terdapat error", "ERROR IMPORT", JOptionPane.ERROR_MESSAGE);
        }
        return null;
    }

    public String hapusCharacter(String text) {
        String textGanti = "";
        try {
            textGanti = text.replaceAll("[^a-zA-Z0-9-_.]", " ");
        } catch (Exception e) {
            textGanti = "";
        }
        return textGanti;
    }

    protected void done() {
        JOptionPane.showMessageDialog(null,"[" + input + "] Proses Import Telah Selesai", "IMPORT", JOptionPane.INFORMATION_MESSAGE);
        System.exit(5);
    }

    public void mohonTunggu() {
        mohonTungguDialog.showMessageDialog(null, "Proses Eksekusi Sedang Berlangsung", "IMPORT", JOptionPane.INFORMATION_MESSAGE);
    }

    public String cekJanganMiripIndexDepo(int indexPembanding) {
        String result = "";
//        if (DataToInsert[mk.getPakSelected()].trim().equalsIgnoreCase("0") == true) {
//        if (DataToInsert[indexPembanding].trim().equalsIgnoreCase("0") == true) {
        if (indexPembanding == 0) {
            result = "";
//        } else if (DataToInsert[indexPembanding].trim().equalsIgnoreCase(DataToInsert[mk.getDepoSelected()].trim()) == true) {
        } else if (indexPembanding == mk.getDepoSelected()) {
            result = "";
        } else {
//            result = "" + DataToInsert[indexPembanding].trim() + "";
            result = hapusCharacter(DataToInsert[indexPembanding].trim());
        }
        return result;
    }

    public String cekJanganMiripIndexDepoNol(int indexPembanding) {
        String result = "";
//        if (DataToInsert[mk.getPakSelected()].trim().equalsIgnoreCase("0") == true) {
//        if (DataToInsert[indexPembanding].trim().equalsIgnoreCase("0") == true) {
        if (indexPembanding == 0) {
            result = "0";
//        } else if (DataToInsert[indexPembanding].trim().equalsIgnoreCase(DataToInsert[mk.getDepoSelected()].trim()) == true) {
        } else if (indexPembanding == mk.getDepoSelected()) {
            result = "0";
        } else {
            result = "" + DataToInsert[indexPembanding].trim() + "";
//            result = hapusCharacter(DataToInsert[indexPembanding].trim());
        }
        return result;
    }

    public void tambahDepoPerbaruan(String depo) {

        try {
            if (!stackDepoPerbaruan.contains(depo)) {
                if (depo.equalsIgnoreCase("NULL")) {

                } else {
                    stackDepoPerbaruan.add(depo);
                }
            }
            System.out.println("STACK: " + stackDepoPerbaruan);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hapusDataLamaBerdepo() {
        try {
            System.out.println(stackDepoPerbaruan.size());
            for (int stackIndex = 0; stackIndex < stackDepoPerbaruan.size(); stackIndex++) {
                System.out.println(stackDepoPerbaruan.get(stackIndex));

                String sql_DeleteSalesBerdepo = "DELETE FROM salesmt "
                        + "WHERE distributor LIKE ? "
                        + "AND depo LIKE ? "
                        + "AND id_sesi_import != ? "
                        + "AND closing = ? "
                        + "AND tahun_closing = ? ";
                PreparedStatement prepared_DeleteSalesBerdepo = con.prepareStatement(sql_DeleteSalesBerdepo);
                prepared_DeleteSalesBerdepo.setString(1, "%" + mDistributor.getNamaDistribbutor() + "%");
                prepared_DeleteSalesBerdepo.setString(2, "%" + stackDepoPerbaruan.get(stackIndex) + "%");
                prepared_DeleteSalesBerdepo.setString(3, sm.getIdSesiImport());
                prepared_DeleteSalesBerdepo.setInt(4, frBaru.getBulanClosing());
                prepared_DeleteSalesBerdepo.setInt(5, frBaru.getTahunClosing());
                System.out.println("DELETE SQL PREPARED : " + prepared_DeleteSalesBerdepo);
                prepared_DeleteSalesBerdepo.executeUpdate();
                prepared_DeleteSalesBerdepo.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String customBulan(int bulanClosing) {
        String bulanClosingCus = "";
        try {
            if (bulanClosing < 10) {
                bulanClosingCus = "0" + bulanClosing;
            } else {
                bulanClosingCus = "" + bulanClosing;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bulanClosingCus;
    }

    public Map kodeBarangTersediaDiKonversiProduk(String kodeBarang, String satuanBarang, String QTYPenjualan) {
        Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
        try {
            String strSelect = "SELECT produk.id AS idmi, produk.kode AS kodemi, produk.nama AS namabarangmi, konversisatuan.satuan, konversisatuan.konversi AS konversikekarton"
                    + " FROM produk "
                    + " INNER JOIN konversisatuan ON konversisatuan.id_produk = produk.id"
                    + " WHERE produk.kode LIKE ?"
                    + " AND konversisatuan.satuan LIKE ?"
                    + " LIMIT 0,1;";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setString(1, kodeBarang);
            preSQL.setString(2, satuanBarang.replaceAll(" ", ""));
            System.out.println("SQL preSQL : " + preSQL);
            float konversiQtyKarton = 0;
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                System.out.println("QTY PENJUALAN : " + QTYPenjualan.replaceAll(",", ".") + " / " + rs.getInt("konversikekarton"));
                if (satuanBarang.replaceAll(" ", "").equalsIgnoreCase("KRT")) {
//                    konversiQtyKarton = Float.parseFloat(QTYPenjualan);
                    konversiQtyKarton = Float.parseFloat(QTYPenjualan.replaceAll(",", "."));
                } else {
                    konversiQtyKarton = Float.parseFloat(QTYPenjualan.replaceAll(",", ".")) / rs.getInt("konversikekarton");
                }
                System.out.println("Konversi Akhir : ");
                System.out.println(konversiQtyKarton);
                System.out.println("ADA");
                rs.beforeFirst();
                while (rs.next()) {
//                    jsonOfKodeBarangDanSatuan.put("iddistributor", rs.getString("id_distributor"));
                    jsonOfKodeBarangDanSatuan.put("idmi", rs.getString("idmi"));
//                    jsonOfKodeBarangDanSatuan.put("namabarangmi", rs.getString("namabarangmi"));
                    jsonOfKodeBarangDanSatuan.put("kodebarangmi", rs.getString("kodemi"));
                    jsonOfKodeBarangDanSatuan.put("satuan", "KRT");
                    jsonOfKodeBarangDanSatuan.put("konversikekarton", rs.getString("konversikekarton"));
                    jsonOfKodeBarangDanSatuan.put("qtykarton", Double.toString(konversiQtyKarton));
                }

            } else {
                System.out.println("TIDAK ADA");
                String idmi = "0";
//                result = kodeBarang;
//                jsonOfKodeBarangDanSatuan.put("iddistributor", Integer.toString(idDistributor));
                String strSelectKodeBarang = "SELECT * FROM produk"
                        + " WHERE kode LIKE ?"
                        + " LIMIT 1;";
                PreparedStatement preSQLProduk = con.prepareStatement(strSelectKodeBarang);
                preSQLProduk.setString(1, kodeBarang);
                System.out.println("SQL preSQL : " + preSQLProduk);
                ResultSet rsProduk = preSQLProduk.executeQuery();
                if (rsProduk.next()) {
                    rsProduk.beforeFirst();
                    while (rsProduk.next()) {
                        idmi = rsProduk.getString("id");
                    }
                }
                jsonOfKodeBarangDanSatuan.put("idmi", idmi);
//                jsonOfKodeBarangDanSatuan.put("namabarangmi", namaBarang);
                jsonOfKodeBarangDanSatuan.put("kodebarangmi", kodeBarang);
                jsonOfKodeBarangDanSatuan.put("satuan", satuanBarang);
                jsonOfKodeBarangDanSatuan.put("konversikekarton", "");
                jsonOfKodeBarangDanSatuan.put("qtykarton", QTYPenjualan);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonOfKodeBarangDanSatuan;
    }

    public void addToStackAllDepo(String depo, String id) {
        try {
            if (!stackAllDepo.contains(depo)) {
                if (depo.equalsIgnoreCase("NULL")) {

                } else {
                    stackAllDepo.add(depo);
                    stackAllIDDepo.add(id);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllDepo(int idDistributor) {
        try {
            String strSelect = "SELECT distributormt.id_distributor, distributormt.nama_distributor, wilayahmt.id_wilayah, wilayahmt.nama_wilayah FROM distributormt "
                    + "RIGHT JOIN wilayahmt ON wilayahmt.id_distributor = distributormt.id_distributor "
                    + "WHERE distributormt.id_distributor = ?";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setInt(1, idDistributor);
            System.out.println("SQL preSQL GET ALL DEPO : " + preSQL);
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    String depoCustom = rs.getString("nama_wilayah");
                    String idDepo = rs.getString("id_wilayah");
                    addToStackAllDepo(depoCustom, idDepo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Map cekAvailableDepo(String depoDiExcel) {
        Map<String, String> jsonOfDataDepo = new HashMap<String, String>();
        System.out.println("depodi EXCEL : " + depoDiExcel);
        try {
            System.out.println("DATA di stackAllDepo : " + stackAllDepo);
            System.out.println("DATA di stackAllIDDepo : " + stackAllIDDepo);
            if (stackAllDepo.contains(depoDiExcel)) {
                System.out.println("DEPO ADA");
                jsonOfDataDepo.put("available", "true");
//                System.out.println();
                String idwil = stackAllIDDepo.get(stackAllDepo.indexOf(depoDiExcel));
                System.out.println("IDWIL : " + idwil);
                jsonOfDataDepo.put("id_wilayah", idwil);
                jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
            } else {
                System.out.println("DEPO TIDAK ADA");
                String strSelect = "SELECT * FROM wilayahmt WHERE nama_wilayah LIKE ?";
                PreparedStatement preSQL = con.prepareStatement(strSelect);
                preSQL.setString(1, "%" + depoDiExcel + "%");
                System.out.println("SQL preSQL GET ALL DEPO : " + preSQL);
                ResultSet rs = preSQL.executeQuery();
                if (rs.next()) {
                    rs.beforeFirst();
                    while (rs.next()) {
                        System.out.println("DEPO ADA");
                        jsonOfDataDepo.put("available", "true");
                        String idwil = Integer.toString(rs.getInt("id_wilayah"));
                        System.out.println("IDWIL : " + idwil);
                        jsonOfDataDepo.put("id_wilayah", idwil);
                        jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
                    }
                } else {
                    jsonOfDataDepo.put("available", "false");
                    jsonOfDataDepo.put("id_wilayah", "");
                    jsonOfDataDepo.put("nama_wilayah", depoDiExcel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("DATA DEPO JSON: " + jsonOfDataDepo);
        return jsonOfDataDepo;
    }

    public void dosPakPcsToKarton(int closing, int tahunClosing) {
        try {
            String strSelect = "SELECT salesmt.id, salesmt.id_sesi_import, salesmt.distributor, salesmt.id_barang, salesmt.kode_barang, salesmt.nama_barang, salesmt.satuan_produk, salesmt.penjualan, salesmt.penjualand, salesmt.dos, salesmt.pak, salesmt.pcs"
                    + " FROM salesmt"
                    //                    + " WHERE closing = 1 AND tahun_closing = '2021'"
                    + " WHERE closing = ? AND tahun_closing = ?"
                    + " AND (salesmt.dos > 0"
                    + " OR salesmt.pak > 0"
                    + " OR salesmt.pcs > 0);";
            PreparedStatement preSQL = con.prepareStatement(strSelect);
            preSQL.setInt(1, closing);
            preSQL.setString(2, Integer.toString(tahunClosing));
            System.out.println("SQL preSQL : " + preSQL);
            ResultSet rs = preSQL.executeQuery();
            int idbaris;
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    float konversiQtyKarton = 0;
                    idbaris = rs.getInt("id");
                    System.out.println("==============================================================");
                    System.out.println("DATA : " + rs.toString());
                    System.out.println("DOSNYA : " + rs.getFloat("dos"));
                    // perhitungan dos
                    if (rs.getFloat("dos") > 0) {
                        konversiQtyKarton += rs.getFloat("dos");
                    }

                    // perhitungan pak
                    System.out.println("PAKNYA : " + rs.getFloat("pak"));
                    if (rs.getFloat("pak") > 0) {
                        String cariKonversiPak = "SELECT * FROM konversisatuan"
                                + " WHERE id_produk = ?"
                                + " AND (satuan LIKE ? OR satuan LIKE 'PAK')"
                                + " LIMIT 1";
                        PreparedStatement preSQLKonversiSatuanPak = con.prepareStatement(cariKonversiPak);
                        preSQLKonversiSatuanPak.setString(1, rs.getString("id_barang"));
                        preSQLKonversiSatuanPak.setString(2, rs.getString("satuan_produk"));

                        float pembagi = 0;
                        float setelahBagi = 0;
                        ResultSet rsKonversiSatuanPak = preSQLKonversiSatuanPak.executeQuery();
                        if (rsKonversiSatuanPak.next()) {
                            rsKonversiSatuanPak.beforeFirst();
                            float nilaiPak = rs.getFloat("pak");
                            while (rsKonversiSatuanPak.next()) {
                                pembagi = rsKonversiSatuanPak.getFloat("konversi");
                                setelahBagi = nilaiPak / pembagi;
                                konversiQtyKarton += setelahBagi;
                            }
                        }
                    }
//                    perhitungan pcs
                    System.out.println("PCSNYA : " + rs.getFloat("pcs"));
                    if (rs.getFloat("pcs") > 0) {
                        String cariKonversiPcs = " SELECT * FROM konversisatuan"
                                + " WHERE id_produk = ?"
                                + " AND (satuan LIKE ? OR satuan LIKE 'PCS')"
                                + " LIMIT 1";
                        PreparedStatement preSQLKonversiSatuanPcs = con.prepareStatement(cariKonversiPcs);
                        preSQLKonversiSatuanPcs.setString(1, rs.getString("id_barang"));
                        preSQLKonversiSatuanPcs.setString(2, rs.getString("satuan_produk"));
                        ResultSet rsKonversiSatuanPcs = preSQLKonversiSatuanPcs.executeQuery();
                        float pembagi = 0;
                        float setelahBagi = 0;
                        if (rsKonversiSatuanPcs.next()) {
                            rsKonversiSatuanPcs.beforeFirst();
                            float nilaiPcs = rs.getFloat("pcs");
                            while (rsKonversiSatuanPcs.next()) {
                                pembagi = rsKonversiSatuanPcs.getFloat("konversi");
                                setelahBagi = nilaiPcs / pembagi;
                                konversiQtyKarton += setelahBagi;
                            }
                        }
                    }
                    System.out.println("NOMINAL PENJUALAN AKHIR (KRT) : " + konversiQtyKarton);
                    System.out.println("=============================================================");
                    System.out.println("UPDATE ke id_sales = " + rs.getString("id"));
                    String sqlUpdate = "UPDATE salesmt SET satuan_produk = 'KRT', penjualan = ?"
                            + " WHERE id = " + rs.getString("id");
                    PreparedStatement preSQLUpdate = con.prepareStatement(sqlUpdate);
                    preSQLUpdate.setFloat(1, konversiQtyKarton);
                    System.out.println("UPDATE : " + preSQLUpdate);
                    preSQLUpdate.executeUpdate();
//                    preSQLUpdate.close();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isClosing(int bulanClosing, int tahunClosing) {
        boolean closing = false;
        try {
            String sql_select = "SELECT * from sales_log_closing_mt "
                    + " WHERE closing = ?"
                    + " AND tahun_closing = ?"
                    + " AND is_closing = 1"
                    + " LIMIT 0,1";
            PreparedStatement preSQLSelect = con.prepareStatement(sql_select);
            preSQLSelect.setInt(1, bulanClosing);
            preSQLSelect.setInt(2, tahunClosing);
            ResultSet rs = preSQLSelect.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                closing = true;
            } else {
                closing = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return closing;
    }
}
