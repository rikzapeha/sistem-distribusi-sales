/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.MasterKonversi;
import Model.MasterKonversiMT;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rikza
 */
public class MasterKonversiMTService {

    Connection connection;
    PreparedStatement preparedStatement;
    MasterKonversiMT masterkonversi;
    List<MasterKonversiMT> arrMasterKonversi = new ArrayList<MasterKonversiMT>();

    public MasterKonversiMTService(Connection connection) {
        this.connection = connection;
    }

    public List<MasterKonversiMT> getAllDepo() throws SQLException {
        preparedStatement = connection.prepareStatement("SELECT * FROM m_convertermt ORDER BY distributor, namaversi ASC");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            masterkonversi = new MasterKonversiMT();
            masterkonversi.setidMKonverter(rs.getInt("id_m_converter"));
            masterkonversi.setDistributor(rs.getString("distributor"));
            masterkonversi.setDistributorIndex(rs.getInt("distributor_index"));
            masterkonversi.setClosing(rs.getInt("closing"));
            masterkonversi.setDepo(rs.getInt("depo"));
            masterkonversi.setTanggal(rs.getInt("tanggal"));
            masterkonversi.setPenjualan(rs.getInt("penjualan"));
            masterkonversi.setNamaVersi(rs.getString("namaversi"));
            arrMasterKonversi.add(masterkonversi);
        }
        return arrMasterKonversi;
    }
    
        public List<MasterKonversiMT> getAllNamaVersinya() throws SQLException {
        preparedStatement = connection.prepareStatement("SELECT * FROM m_convertermt");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            masterkonversi = new MasterKonversiMT();
            masterkonversi.setidMKonverter(rs.getInt("id_m_converter"));
            masterkonversi.setDistributor(rs.getString("distributor"));
            masterkonversi.setDistributorIndex(rs.getInt("distributor_index"));
            masterkonversi.setClosing(rs.getInt("closing"));
            masterkonversi.setDepo(rs.getInt("depo"));
            masterkonversi.setTanggal(rs.getInt("tanggal"));
            masterkonversi.setPenjualan(rs.getInt("penjualan"));
            arrMasterKonversi.add(masterkonversi);
        }
        return arrMasterKonversi;
    }

    public List<MasterKonversiMT> getKonverter(int idMasterKonverter) throws SQLException {
        masterkonversi = new MasterKonversiMT();
        preparedStatement = connection.prepareStatement("SELECT * FROM m_convertermt WHERE id_m_converter = '" + idMasterKonverter + "'");
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            masterkonversi.setidMKonverterSelected(rs.getInt("id_m_converter"));
            masterkonversi.setDistributorSelected(rs.getString("distributor"));
            masterkonversi.setDistributorIndexSelected(rs.getInt("distributor_index"));
            masterkonversi.setClosingSelected(rs.getInt("closing"));
            masterkonversi.setDepoSelected(rs.getInt("depo"));
            masterkonversi.setTanggalSelected(rs.getInt("tanggal"));
            masterkonversi.setPenjualanSelected(rs.getInt("penjualan"));
            masterkonversi.setNotaSelected(rs.getInt("nota"));
            masterkonversi.setKodeCustomerSelected(rs.getInt("kode_customer"));
            masterkonversi.setNamaCustomerSelected(rs.getInt("nama_customer"));
            masterkonversi.setFrekuensiSelected(rs.getInt("frekuensi"));
            masterkonversi.setAlamatSelected(rs.getInt("alamat"));
            masterkonversi.setPasarSelected(rs.getInt("pasar"));
            masterkonversi.setKecamatanSelected(rs.getInt("kecamatan"));
            masterkonversi.setKabupatenSelected(rs.getInt("kabupaten"));
            masterkonversi.setKodeBarangSelected(rs.getInt("kode_barang"));
            masterkonversi.setNamaBarangSelected(rs.getInt("nama_barang"));
            masterkonversi.setSatuanProdukSelected(rs.getInt("satuan_produk"));
            masterkonversi.setNamaSalesmanSelected(rs.getInt("nama_salesman"));
            masterkonversi.setRupiahSelected(rs.getInt("rupiah"));
            masterkonversi.setDosSelected(rs.getInt("dos"));
            masterkonversi.setPakSelected(rs.getInt("pak"));
            masterkonversi.setPcsSelected(rs.getInt("pcs"));
            arrMasterKonversi.add(masterkonversi);
        }
        return arrMasterKonversi;
    }
}
