/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package example;

import Controller.PerbaikiDataController;
import Model.MasterKonversi;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import utility.Koneksi;
import utility.KoneksiDB;

/**
 *
 * @author rikza
 */
public class RikzaTesting {

    Connection connection;
    PreparedStatement preparedStatement;

    public static void main(String[] args) throws SQLException, IOException, InterruptedException {
//        String cobaa = "BANJAR*MAS#IN";
//        cobaa = cobaa.replaceAll("[^a-zA-Z0-9]", " ");
//        System.out.println(cobaa);
        RikzaTesting rs = new RikzaTesting();
//        rs.perbaikanWilayah();
//        rs.perbaikiSalesman();
//        rs.verifikasiProdukDariMYERP();
//        rs.verifikasiKodeBarangBerdasarkanKolomAlias();
//        rs.nyamakanKodeBarang();
//        rs.verifikasiSalesman();
//        rs.verifikasiTargetSales();
//        rs.ubahKodeDefault();
//        rs.dapatkanKodeMI();
//        rs.tambahKonversiProdukPerDistributor();
        System.out.println("HALO RIKZA TESTING");
//        System.out.println("DATA : " + rs.kodeBarangTersediaDiKonversiProduk("1106FG-LDK33", "PCS", "100"));

//        String depoDiExcel = "1~MADIUN";
//        String[] depoSplit = depoDiExcel.split("~");
//        System.out.println("DATANYA : " + depoSplit[0] + " dan " + depoSplit[1]);
//        rs.dosPakPcsToKarton();
//      Runtime.getRuntime().exec("c");
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

    public RikzaTesting() {
        this.connection = KoneksiDB.getKoneksi();
    }

    public RikzaTesting(Connection connection) {
        this.connection = connection;
    }

    public void perbaikanWilayah() throws SQLException {
        String sql_tampilkan = "SELECT a.depo, b.id_wilayah FROM sales a INNER JOIN wilayah b ON a.depo = b.nama_wilayah WHERE (b.nama_wilayah IS NOT NULL=TRUE) or a.id_depo='' or a.id_depo=0 or a.id_depo=a.depo GROUP BY a.depo";
        System.out.println(sql_tampilkan);
        preparedStatement = connection.prepareStatement(sql_tampilkan);
        ResultSet rs = preparedStatement.executeQuery();
        while (rs.next()) {
            System.out.println(rs.getString("depo") + " - " + rs.getString("id_wilayah"));
            String sql_update = "UPDATE sales SET id_depo = " + rs.getString("id_wilayah") + " WHERE depo = '" + rs.getString("depo") + "'";
            if (rs.getString("id_wilayah").equals("null")) {
                System.out.println("ADA NULL " + sql_update);
            } else {
                System.out.println("TIDAK NULL " + sql_update);
                preparedStatement = connection.prepareStatement(sql_update);
                preparedStatement.executeUpdate();
            }
        }
    }

    public void perbaikiSalesman() {
        try {
            String sql_tampilkan = "SELECT a.nama_salesman, b.id FROM sales a INNER JOIN salesman b ON a.nama_salesman = b.namasales WHERE (b.namasales IS NOT NULL=TRUE) or a.id_sales='' or a.id_sales=0 or a.id_sales=a.nama_salesman GROUP BY a.nama_salesman";
            System.out.println(sql_tampilkan);
            preparedStatement = connection.prepareStatement(sql_tampilkan);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("nama_salesman") + " - " + rs.getString("id"));
                String sql_update = "UPDATE sales SET id_sales = " + rs.getString("id") + " WHERE nama_salesman = '" + rs.getString("nama_salesman") + "'";
                if (rs.getString("id").equals("null")) {
                    System.out.println("ADA NULL " + sql_update);
                } else {
                    System.out.println("TIDAK NULL " + sql_update);
                    preparedStatement = connection.prepareStatement(sql_update);
                    preparedStatement.executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifikasiProdukDariMYERP() {
        try {
            String sql_tampilkan = "SELECT bkode, bnama FROM item_dumy";
            System.out.println(sql_tampilkan);
            preparedStatement = connection.prepareStatement(sql_tampilkan);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("bkode") + " - " + rs.getString("bnama"));
                String sql_update = "SELECT * FROM produk WHERE kode LIKE '%" + rs.getString("bkode") + "%' LIMIT 1";
                PreparedStatement preparedStatement_cek = connection.prepareStatement(sql_update);
                ResultSet rs_cek = preparedStatement_cek.executeQuery();
                System.out.println(sql_update);
                if (rs_cek.next()) {
//                    rs_cek.beforeFirst();
                    System.out.println("--ada, datanyanya " + rs_cek.getString("kode"));
                } else {
                    System.out.println("--tidak ada, maka ditambahkan");
                    String sql_insert = "INSERT into produk (kode, nama) VALUES (?,?)";
                    PreparedStatement preSQLInsert = connection.prepareStatement(sql_insert);
                    preSQLInsert.setString(1, rs.getString("bkode"));
                    preSQLInsert.setString(2, rs.getString("bnama"));
                    System.out.println("QUERY INSERT : " + preSQLInsert);
                    preSQLInsert.execute();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifikasiKodeBarangBerdasarkanKolomAlias() {
        try {
            String sql_tampilkan = "SELECT * FROM distributor";
            System.out.println(sql_tampilkan);
            preparedStatement = connection.prepareStatement(sql_tampilkan);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    System.out.println("- verifikasi distributor : " + rs.getString("nama_distributor") + " dan aliasnya : " + rs.getString("alias"));

                }
            }

        } catch (Exception e) {

        }
    }

    public void nyamakanKodeBarang() {
        try {
            String sql_tampilkan = "SELECT * FROM item_dumy";
            System.out.println(sql_tampilkan);
            preparedStatement = connection.prepareStatement(sql_tampilkan);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                System.out.println("- nama barang : " + rs.getString("bnama"));
                String update_sql = "UPDATE masterbarang_produk SET kode_barang_motasa = ? WHERE nama_barang LIKE '%" + rs.getString("bnama") + "%'";
                PreparedStatement preSQLUpdate = connection.prepareStatement(update_sql);
                preSQLUpdate.setString(1, rs.getString("bkode"));
                System.out.println("SQL UPDATE : " + preSQLUpdate);
                preSQLUpdate.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifikasiSalesman() {
        try {
            String sql_select = "SELECT salesman_dumy.*, distributor.nama_distributor, distributor.id_distributor AS id_dist_asli FROM salesman_dumy INNER JOIN distributor "
                    + "ON distributor.nama_distributor = salesman_dumy.distributor "
                    + "WHERE salesman_dumy.distributor = distributor.nama_distributor ";
//            String sql_select = "SELECT salesman_dumy.*, distributor.nama_distributor, salesman.id AS id_salesman, salesman.namasales  FROM salesman_dumy "
//                    + "INNER JOIN distributor ON distributor.nama_distributor = salesman_dumy.distributor "
//                    + "INNER JOIN salesman ON salesman.id_distributor = distributor.id_distributor "
//                    + "WHERE salesman_dumy.distributor = distributor.nama_distributor "
//                    + "AND salesman.namasales LIKE salesman_dumy.nama_salesman";
            System.out.println(sql_select);
            preparedStatement = connection.prepareStatement(sql_select);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    System.out.println("-DATANYA : " + rs.getString("distributor") + " NAMA SALES : " + rs.getString("nama_salesman") + " ID DIstr :   " + rs.getInt("id_dist_asli"));
//                    String sql_updateSalesName = "UPDATE salesman SET nama_asli = '" + rs.getString("nama_salesman") + "' WHERE id = " + rs.getInt("id_salesman");
//                    PreparedStatement preUpdateSalesName = connection.prepareStatement(sql_updateSalesName);
//                    preUpdateSalesName.executeUpdate();
//                    System.out.println(sql_updateSalesName);

//                    CARA KEDUA
                    String sql_cariNamaSales = "SELECT * FROM salesman WHERE namasales LIKE '%" + rs.getString("nama_salesman").trim() + "%'" + " AND id_distributor = " + rs.getInt("id_dist_asli");
                    System.out.println("-- cariNamaSales : " + sql_cariNamaSales);
                    PreparedStatement preSelectNamaSales = connection.prepareStatement(sql_cariNamaSales);
                    ResultSet rsSelectNamaSales = preSelectNamaSales.executeQuery();
                    if (rsSelectNamaSales.next()) {
                        System.out.println("--- DATA SALESMAN -> nama sales : " + rsSelectNamaSales.getString("namasales").trim() + " ID baris : " + rsSelectNamaSales.getInt("id") + " ID dist : " + rsSelectNamaSales.getInt("id_distributor"));
                        rsSelectNamaSales.beforeFirst();
                        while (rsSelectNamaSales.next()) {
                            String sql_updateSalesName = "UPDATE salesman SET nama_asli = '" + rs.getString("nama_salesman").trim() + "' WHERE id = " + rsSelectNamaSales.getString("id") + " AND id_distributor = " + rs.getInt("id_dist_asli");
                            System.out.println("---- " + sql_updateSalesName);
                            PreparedStatement preUpdateNamaSales = connection.prepareStatement(sql_updateSalesName);
                            preUpdateNamaSales.executeUpdate();
                        }
                    }
                    System.out.println("");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verifikasiTargetSales() {
        try {
            String sql_select = "SELECT salesman.id, salesman.namasales, salesman.nama_asli, "
                    + "salesman.id_distributor, salesman_dumy.target_ladaku, salesman_dumy.target_ketumbar, "
                    + "salesman_dumy.target_kunyit, salesman_dumy.target_seasoning, salesman_dumy.target_marinasi "
                    + "FROM salesman INNER JOIN salesman_dumy ON salesman_dumy.nama_salesman = salesman.nama_asli";
            PreparedStatement preStatement_select = connection.prepareStatement(sql_select);
            ResultSet rs_select = preStatement_select.executeQuery();
            if (rs_select.next()) {
                rs_select.beforeFirst();
                while (rs_select.next()) {
                    int kodeLadaku = this.getIdDariProduk("LDK33");
                    int kodeKetumbar = this.getIdDariProduk("DSK07");
                    int kodeKunyit = this.getIdDariProduk("DKU02");
                    int kodeSeasoning = this.getIdDariProduk("DBL01");
                    int kodeMarinasi = this.getIdDariProduk("DMR01");
//                    1. insert target ladaku, input januari sampai maret dulu
                    for (int i = 1; i <= 3; i++) {
                        String sql_insertTarget = "INSERT into targetsalesman (id_sales, periode, tahun, id_item, targetao) "
                                + "VALUES (?,?,?,?,?)";
                        PreparedStatement prepared_insertTarget = connection.prepareStatement(sql_insertTarget);
                        prepared_insertTarget.setInt(1, rs_select.getInt("id"));
                        prepared_insertTarget.setInt(2, i);
                        prepared_insertTarget.setInt(3, 2021);
                        prepared_insertTarget.setInt(4, kodeLadaku);
                        prepared_insertTarget.setInt(5, rs_select.getInt("target_ladaku"));
                        System.out.println("----> INSERT nya Ladaku: " + prepared_insertTarget);
                        prepared_insertTarget.executeUpdate();
                    }
//                    2. insert target ketumbar
                    for (int i = 1; i <= 3; i++) {
                        String sql_insertTarget = "INSERT into targetsalesman (id_sales, periode, tahun, id_item, targetao) "
                                + "VALUES (?,?,?,?,?)";
                        PreparedStatement prepared_insertTarget = connection.prepareStatement(sql_insertTarget);
                        prepared_insertTarget.setInt(1, rs_select.getInt("id"));
                        prepared_insertTarget.setInt(2, i);
                        prepared_insertTarget.setInt(3, 2021);
                        prepared_insertTarget.setInt(4, kodeKetumbar);
                        prepared_insertTarget.setInt(5, rs_select.getInt("target_ketumbar"));
                        System.out.println("----> INSERT nya Ketumbar: " + prepared_insertTarget);
                        prepared_insertTarget.executeUpdate();
                    }
//                    3. insert target kunyit
                    for (int i = 1; i <= 3; i++) {
                        String sql_insertTarget = "INSERT into targetsalesman (id_sales, periode, tahun, id_item, targetao) "
                                + "VALUES (?,?,?,?,?)";
                        PreparedStatement prepared_insertTarget = connection.prepareStatement(sql_insertTarget);
                        prepared_insertTarget.setInt(1, rs_select.getInt("id"));
                        prepared_insertTarget.setInt(2, i);
                        prepared_insertTarget.setInt(3, 2021);
                        prepared_insertTarget.setInt(4, kodeKunyit);
                        prepared_insertTarget.setInt(5, rs_select.getInt("target_kunyit"));
                        System.out.println("----> INSERT nya Kunyit: " + prepared_insertTarget);
                        prepared_insertTarget.executeUpdate();
                    }
//                    4. insert target seasoning
                    for (int i = 1; i <= 3; i++) {
                        String sql_insertTarget = "INSERT into targetsalesman (id_sales, periode, tahun, id_item, targetao) "
                                + "VALUES (?,?,?,?,?)";
                        PreparedStatement prepared_insertTarget = connection.prepareStatement(sql_insertTarget);
                        prepared_insertTarget.setInt(1, rs_select.getInt("id"));
                        prepared_insertTarget.setInt(2, i);
                        prepared_insertTarget.setInt(3, 2021);
                        prepared_insertTarget.setInt(4, kodeSeasoning);
                        prepared_insertTarget.setInt(5, rs_select.getInt("target_seasoning"));
                        System.out.println("----> INSERT nya Kunyit: " + prepared_insertTarget);
                        prepared_insertTarget.executeUpdate();
                    }
//                    5. insert target marinasi
                    for (int i = 1; i <= 3; i++) {
                        String sql_insertTarget = "INSERT into targetsalesman (id_sales, periode, tahun, id_item, targetao) "
                                + "VALUES (?,?,?,?,?)";
                        PreparedStatement prepared_insertTarget = connection.prepareStatement(sql_insertTarget);
                        prepared_insertTarget.setInt(1, rs_select.getInt("id"));
                        prepared_insertTarget.setInt(2, i);
                        prepared_insertTarget.setInt(3, 2021);
                        prepared_insertTarget.setInt(4, kodeMarinasi);
                        prepared_insertTarget.setInt(5, rs_select.getInt("target_marinasi"));
                        System.out.println("----> INSERT nya Kunyit: " + prepared_insertTarget);
                        prepared_insertTarget.executeUpdate();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getIdDariProduk(String kodeItem) {
        int idItem = 0;
        try {
            String sql = "SELECT id FROM produk WHERE kode LIKE '%" + kodeItem + "%' LIMIT 1";
            PreparedStatement preSQL = connection.prepareStatement(sql);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                idItem = rs.getInt("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("idItem : " + idItem);
        return idItem;
    }

    public void tambahkanAlias() {
        try {
            String strSelect = "SELECT item_dumy.bkode, item_dumy.alias, produk.id as idproduk, produk.kode AS kodediproduk, produk.alias_report FROM item_dumy\n"
                    + "INNER JOIN produk ON produk.kode = item_dumy.bkode";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                String sqlUpdate = "UPDATE produk SET alias_report ='" + rs.getString("alias") + "' WHERE id=" + rs.getString("idproduk");
                PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                preSQLUpdate.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ubahKodeDefault() {
        try {
            String strSelect = "SELECT * FROM sales "
                    + "WHERE distributor LIKE '%BINTANG BALI INDAH%'";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                System.out.println("KODE BARANG d = " + rs.getString("kode_barangd"));
                String sqlUpdate = "UPDATE sales SET kode_barangd ='" + rs.getString("kode_barang") + "' WHERE id=" + rs.getString("id");
                PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                preSQLUpdate.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    step 1
    public void dapatkanKodeMI() {
        try {
            String strSelect = "SELECT * FROM konversiproduk";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                String sqlUpdate = "UPDATE sales SET kode_barang='" + rs.getString("kodemi") + "' WHERE id_distributor=" + rs.getInt("id_distributor") + " "
                        + "AND (kode_barangd LIKE '" + rs.getString("kode") + "' OR nama_barangd LIKE '" + rs.getString("nama") + "')";
                PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                System.out.println("Step1 : " + "baris ke: " + rs.getInt("id") + "/" + preSQLUpdate);
                preSQLUpdate.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//    step 2

    public void isiIdBarang() {
        try {
            String strSelect = "SELECT * FROM produk";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                String sqlUpdate = "UPDATE sales SET id_barang='" + rs.getInt("id") + "' WHERE kode_barang LIKE '" + rs.getString("kode") + "'";
                PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                System.out.println("Step2: " + preSQLUpdate);
                preSQLUpdate.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tambahKonversiProdukPerDistributor() {
        try {
            String strSelect = "SELECT a.kode_barangd,a.nama_barangd,a.id_distributor,a.distributor "
                    + "FROM viewbarangd a "
                    + "LEFT JOIN konversiproduk b ON a.id_distributor=b.id_distributor and a.kode_barangd =b.kode AND a.nama_barangd=b.nama "
                    + "WHERE b.kode IS NULL=TRUE "
                    + "AND a.kode_barangd LIKE '1106FG-%'";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            ResultSet rs = preSQL.executeQuery();
            while (rs.next()) {
                String cek_available = "SELECT * FROM produk WHERE kode LIKE '" + rs.getString("kode_barangd") + "' LIMIT 1";
                PreparedStatement preSQLCek = connection.prepareStatement(cek_available);
                ResultSet rsCek = preSQLCek.executeQuery();
                while (rsCek.next()) {
                    String sqlUpdate = "INSERT INTO konversiproduk (id_distributor, kode, nama, kodemi) "
                            + "VALUES (?,?,?,?)";
                    PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                    preSQLUpdate.setInt(1, rs.getInt("id_distributor"));
                    preSQLUpdate.setString(2, rs.getString("kode_barangd"));
                    preSQLUpdate.setString(3, rs.getString("nama_barangd"));
                    preSQLUpdate.setString(4, rsCek.getString("kode"));
                    System.out.println("INSERT : " + preSQLUpdate);
                    preSQLUpdate.executeUpdate();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Map kodeBarangTersediaDiKonversiProduk(String kodeBarang, String satuanBarang, String QTYPenjualan) {
        Map<String, String> jsonOfKodeBarangDanSatuan = new HashMap<String, String>();
        try {
            String strSelect = "SELECT produk.id AS idmi, produk.kode AS kodemi, produk.nama AS namabarangmi, konversisatuan.satuan, konversisatuan.konversi AS konversikekarton \n"
                    + " FROM produk "
                    + " INNER JOIN konversisatuan ON konversisatuan.id_produk = produk.id"
                    + " WHERE produk.kode LIKE ?"
                    + " AND konversisatuan.satuan LIKE ?"
                    + " LIMIT 0,1;";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            preSQL.setString(1, kodeBarang);
            preSQL.setString(2, satuanBarang);
            System.out.println("SQL preSQL : " + preSQL);
            float konversiQtyKarton = 0;
            ResultSet rs = preSQL.executeQuery();
            if (rs.next()) {
                System.out.println("QTY PENJUALAN : " + QTYPenjualan + " / " + rs.getInt("konversikekarton"));
                konversiQtyKarton = Float.parseFloat(QTYPenjualan) / rs.getInt("konversikekarton");
                System.out.println("qtyKarton : " + konversiQtyKarton);
                System.out.println("ADA");
                rs.beforeFirst();
                while (rs.next()) {
//                    jsonOfKodeBarangDanSatuan.put("iddistributor", rs.getString("id_distributor"));
                    jsonOfKodeBarangDanSatuan.put("idmi", rs.getString("idmi"));
//                    jsonOfKodeBarangDanSatuan.put("namabarangmi", rs.getString("namabarangmi"));
                    jsonOfKodeBarangDanSatuan.put("kodebarangmi", rs.getString("kodemi"));
                    jsonOfKodeBarangDanSatuan.put("satuan", "KRT");
                    jsonOfKodeBarangDanSatuan.put("konversikekarton", rs.getString("konversikekarton"));
                    jsonOfKodeBarangDanSatuan.put("qtykarton", Float.toString(konversiQtyKarton));
                }

            } else {
                System.out.println("TIDAK ADA");
//                result = kodeBarang;
//                jsonOfKodeBarangDanSatuan.put("iddistributor", Integer.toString(idDistributor));
                jsonOfKodeBarangDanSatuan.put("idmi", "");
//                jsonOfKodeBarangDanSatuan.put("namabarangmi", namaBarang);
                jsonOfKodeBarangDanSatuan.put("kodebarangmi", kodeBarang);
                jsonOfKodeBarangDanSatuan.put("satuan", satuanBarang);
                jsonOfKodeBarangDanSatuan.put("konversikekarton", "");
                jsonOfKodeBarangDanSatuan.put("qtykarton", QTYPenjualan);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonOfKodeBarangDanSatuan;
    }

    public void dosPakPcsToKarton() {
        try {
            String strSelect = "SELECT sales.id, sales.id_sesi_import, sales.distributor, sales.id_barang, sales.kode_barang, sales.nama_barang, sales.satuan_produk, sales.penjualan, sales.penjualand, sales.dos, sales.pak, sales.pcs"
                    + " FROM sales"
                    + " WHERE closing = 1 AND tahun_closing = '2021'"
                    + " AND (sales.dos > 0"
                    + " OR sales.pak > 0"
                    + " OR sales.pcs > 0);";
            PreparedStatement preSQL = connection.prepareStatement(strSelect);
            System.out.println("SQL preSQL : " + preSQL);
            ResultSet rs = preSQL.executeQuery();
            int idbaris;
            if (rs.next()) {
                rs.beforeFirst();
                while (rs.next()) {
                    float konversiQtyKarton = 0;
                    idbaris = rs.getInt("id");
                    System.out.println("==============================================================");
                    System.out.println("DATA : " + rs.toString());
                    System.out.println("DOSNYA : " + rs.getFloat("dos"));
                    // perhitungan dos
                    if (rs.getFloat("dos") > 0) {
                        konversiQtyKarton += rs.getFloat("dos");
                    }

                    // perhitungan pak
                    System.out.println("PAKNYA : " + rs.getFloat("pak"));
                    if (rs.getFloat("pak") > 0) {
                        String cariKonversiPak = "SELECT * FROM konversisatuan"
                                + "WHERE id_produk = ?"
                                + "AND (satuan LIKE ? OR satuan LIKE 'PAK')"
                                + "LIMIT 1";
                        PreparedStatement preSQLKonversiSatuanPak = connection.prepareStatement(cariKonversiPak);
                        preSQLKonversiSatuanPak.setString(1, rs.getString("id_barang"));
                        preSQLKonversiSatuanPak.setString(2, rs.getString("satuan_produk"));

                        float pembagi = 0;
                        float setelahBagi = 0;
                        ResultSet rsKonversiSatuanPak = preSQLKonversiSatuanPak.executeQuery();
                        if (rsKonversiSatuanPak.next()) {
                            rsKonversiSatuanPak.beforeFirst();
                            float nilaiPak = rs.getFloat("pak");
                            while (rsKonversiSatuanPak.next()) {
                                pembagi = rsKonversiSatuanPak.getFloat("konversi");
                                setelahBagi = nilaiPak / pembagi;
                                konversiQtyKarton += setelahBagi;
                            }
                        }
                    }
//                    perhitungan pcs
                    System.out.println("PCSNYA : " + rs.getFloat("pcs"));
                    if (rs.getFloat("pcs") > 0) {
                        String cariKonversiPcs = " SELECT * FROM konversisatuan"
                                + " WHERE id_produk = ?"
                                + " AND (satuan LIKE ? OR satuan LIKE 'PCS')"
                                + " LIMIT 1";
                        PreparedStatement preSQLKonversiSatuanPcs = connection.prepareStatement(cariKonversiPcs);
                        preSQLKonversiSatuanPcs.setString(1, rs.getString("id_barang"));
                        preSQLKonversiSatuanPcs.setString(2, rs.getString("satuan_produk"));
                        ResultSet rsKonversiSatuanPcs = preSQLKonversiSatuanPcs.executeQuery();
                        float pembagi = 0;
                        float setelahBagi = 0;
                        if (rsKonversiSatuanPcs.next()) {
                            rsKonversiSatuanPcs.beforeFirst();
                            float nilaiPcs = rs.getFloat("pcs");
                            while (rsKonversiSatuanPcs.next()) {
                                pembagi = rsKonversiSatuanPcs.getFloat("konversi");
                                setelahBagi = nilaiPcs / pembagi;
                                konversiQtyKarton += setelahBagi;
                            }
                        }
                    }
                    System.out.println("NOMINAL PENJUALAN AKHIR (KRT) : " + konversiQtyKarton);
                    System.out.println("=============================================================");
                    System.out.println("UPDATE ke id_sales = " + rs.getString("id"));
                    String sqlUpdate = "UPDATE sales SET satuan_produk = 'KRT', penjualan = ?"
                            + " WHERE id = " + rs.getString("id");
                    PreparedStatement preSQLUpdate = connection.prepareStatement(sqlUpdate);
                    preSQLUpdate.setFloat(1, konversiQtyKarton);
                    System.out.println("UPDATE : " + preSQLUpdate);
                    preSQLUpdate.executeUpdate();

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
